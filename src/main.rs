#![deny(warnings)]
#[allow(dead_code)]

extern crate base64;
extern crate reqwest;
extern crate serde_json;
extern crate regex;

use std::collections::HashMap;
use reqwest::Response;
use std::{thread, time};
use std::env;
use regex::Regex;
use std::io::{Read};
use std::fs::File;
use serde_json::{Value};

fn main() {

    let path = env::current_dir().unwrap();
    println!("Starting bot in {}", path.display());

    //Get token
    let token = &get_token() as &str;
    println!("Got access token!");

    let _user = get_user(token);
    println!("Got user details!");

    println!("Starting scanning...");

    loop {

        let comments = get_comments();

        let comment_data = &comments["data"]["children"];

        for comment_id in 0..100 {
            let comment = &comment_data[comment_id]["data"];

            let body = format!("{}", comment["body"]);

            if !body.contains("https://www.google.com/amp/") && !body.contains("https://amp.") {
                continue;
            }

            send_comment(comment.clone(), token);
        }

        //Sleep
        let millis = time::Duration::from_millis(2000);
        let now = time::Instant::now();

        thread::sleep(millis);

        assert!(now.elapsed() >= millis);
    }
}

fn get_comments()-> serde_json::Value {

    let mut res = get_request("https://api.reddit.com/r/all/comments.json?limit=100", None);

    if res.status().as_str().trim() != "200" {
        println!("Error occurred when getting comments... Code {}", res.text().unwrap().as_str().trim());
        std::process::exit(0);
    }

    let json: serde_json::Value = res.json().unwrap();

    return json;
}

fn send_comment(comment: serde_json::Value, token: &str) {

    let mut message: String = String::new();

    //Find links
    let re = Regex::new(r"https://([\w_-]+(?:(?:\.[\w_-]+)+))([\w.,@?^=%&:/~+#-]*[\w@?^=%&/~+#-])?").unwrap();
    let text = &format!("{}", comment["body"]);

    let mut link_count = 1;
    let mut skip = false;

    for cap in re.captures_iter(text) {

        //Skip every second one
        if skip {
            skip = false;
            continue;
        } else {
            skip = true;
        }

        let res = reqwest::Client::new()
            .get(&cap[0].replace("://amp.", "://"))
            .header("User-Agent", "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:65.0) Gecko/20100101 Firefox/65.0")
            .send().unwrap();

        if res.status().as_str().trim() != "200" {
            continue;
        }

        message += &format!("[Non Amp Link {}]( {} )\n\n", link_count, res.url());
        link_count += 1;
    }

    message += &String::from("*Bleep Bloop this action was performed automatically. Mods please ban me to remove me from this subreddit.*");

    submit_comment(&format!("{}", &comment["name"].to_string()[1..11].to_string()).as_str(), message.as_str(), token);}

fn submit_comment(parent: &str, text: &str, token: &str)-> serde_json::Value {

    let url = "https://oauth.reddit.com/api/comment";

    let mut params = HashMap::new();
    params.insert("api_type", "json");
    params.insert("text", text);
    params.insert("thing_id", parent);
    params.insert("sr", "UnAmpBot");
    params.insert("uh", token);

    let mut res = send_request(url, Some(params), token);

    if res.status().as_str().trim() != "200" {
        println!("Error occurred when submitting comment... Code {}", res.status().as_str().trim());
        std::process::exit(0);
    }

    let json: serde_json::Value = res.json().unwrap();

    let link = &json["json"]["data"]["things"][0]["data"]["permalink"];

    println!("Submitted comment at https://reddit.com{}", link.as_str().unwrap());

    return json;
}

fn get_token()-> String {

    let mut file = File::open("settings.json").unwrap();
    let mut contents = String::new();
    file.read_to_string(&mut contents).unwrap();
    let data: Value = serde_json::from_str(&contents).unwrap();


    let url = "https://www.reddit.com/api/v1/access_token";

    let mut params = HashMap::new();
    params.insert("grant_type", "password");
    params.insert("password", data["password"].as_str().unwrap());
    params.insert("username", &(data["username"].as_str().unwrap()));

    let mut res = reqwest::Client::new()
        .post(url)
        .header("User-Agent", "UnAmpBot 0.0.1 (Rust) by /u/monster4210 ")
        //Username:Password base64 encoded
        .header("Authorization", format!("Basic {}", base64::encode(data["secret"].as_str().unwrap())))
        .form(&params)
        .send()
        .unwrap();

    if res.status().as_str().trim() != "200" {
        println!("Error occurred when getting key... Code {}", res.status().as_str().trim());
        std::process::exit(0);
    }

    let json: serde_json::Value = res.json().unwrap();

    return json["access_token"].to_string()[1..41].to_string();
}

fn get_user(token: &str)-> serde_json::Value {
    let mut user = get_request("https://oauth.reddit.com/api/v1/me", Some(token));

    if user.status().as_str().trim() != "200" {
        println!("Error occurred when getting user... Code {}", user.status().as_str().trim());
        std::process::exit(0);
    }

    return user.json().unwrap();
}

fn send_request(url: &str, params: Option<HashMap<&str, &str>>, token: &str)-> Response {
    let mut req = reqwest::Client::new()
        .post(url)
        .header("User-Agent", "UnAmpBot 0.0.1 (Rust) by /u/monster4210 ")
        .header("Authorization", format!("bearer {}", token));

    if params != None {
        req = req.form(&params.unwrap());
    }

    let res = req.send().unwrap();

    return res;
}

fn get_request(url: &str, token: Option<&str>)-> Response {
    let mut req = reqwest::Client::new()
        .get(url)
        .header("User-Agent", "UnAmpBot 0.0.1 (Rust) by /u/monster4210 ");

    if token != None {
        req = req.header("Authorization", format!("bearer {}", token.unwrap()));
    }

    let res = req.send().unwrap();

    return res;
}